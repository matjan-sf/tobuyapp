import angular from 'angular';
import Navbar from './navbar/navbar.component';
import User from './user/user';
import Footer from './footer/footer.component';

let commonModule = angular.module('app.common', [
  Navbar,
  User,
  Footer
])

.name;

export default commonModule;
