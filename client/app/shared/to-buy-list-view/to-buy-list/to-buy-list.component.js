import angular from 'angular'
import template from './to-buy-list.html'
import uiRouter from 'angular-ui-router';


class toBuyListController {
    constructor(){


    }
}

export default angular.module('toBuyList', [uiRouter])
    .component('toBuyList', {
        controller: toBuyListController,
        controllerAs: 'vm',
        template,
        bindings: {}
    })
    .name
