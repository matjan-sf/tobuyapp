import angular from 'angular';
import template from './to-buy-form.html'
import uiRouter from 'angular-ui-router';

class ToBuyFormController{
    constructor(){

    }
}
export default angular.module('toBuyForm', [uiRouter])
.component('toBuyForm', {
    controller: ToBuyFormController,
    controllerAs: 'vm',
    template,
    bindings: {},
})
.name

