import angular from 'angular'
import uiRoute from 'angular-ui-router'
import template from './to-buy-list-view.html'

class totoBuyListViewController {
    /*@ngInject*/

    constructor(){


    }
}

export default angular.module('toBuyListView', [uiRoute])
.component('toBuyListView', {
    controller: totoBuyListViewController,
    controllerAs: 'vm',
    template,
    bindings: {}
})
.name
