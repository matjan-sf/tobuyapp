import angular from 'angular'
import uiRouter from 'angular-ui-router';
import template from './to-buy-notes.html'

class ToBuyNotesController {
    constructor(){


    }
}

export default angular.module('toBuyNotes', [uiRouter])
    .component('toBuyNotes', {
        controller: ToBuyNotesController,
        controllerAs: 'vm',
        template,
        bindings: {}
    })
    .name
