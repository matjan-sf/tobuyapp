import angular from 'angular';
import toBuyForm from './to-buy-list-view/to-buy-form/to-buy-form.component';
import toBuyList from './to-buy-list-view/to-buy-list/to-buy-list.component';
import toBuyListView from './to-buy-list-view/to-buy-list-view.component';
import toBuyNotes from './to-buy-list-view/to-buy-notes/to-buy-notes.component';




let sharedModule = angular.module('app.shared', [
    toBuyForm,
    toBuyList,
    toBuyListView,
    toBuyNotes



])
    .name;

export default sharedModule;
