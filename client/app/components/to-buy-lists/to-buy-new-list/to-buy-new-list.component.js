import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './to-buy-new-list.html';


class ToBuyNewListController {
    /*@ngInject*/
    constructor() {
   };

}

export default angular.module('toBuyNewList', [uiRouter])

    .component('toBuyNewList',{
        template,
        controller: ToBuyNewListController,
        controllerAs: 'vm',
        bindings: {}
    })

    .name;





