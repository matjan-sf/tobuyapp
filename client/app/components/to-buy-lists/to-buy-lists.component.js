import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './to-buy-lists.html';
import dataService from '../../service/dataService.service';


class ToBuyListsController {
    toBuyLists = '';
    /*@ngInject*/
    constructor(dataService) {
        const vm  = this;

        dataService.getData().then((data) => {vm.toBuyLists = data.toBuyLists;}, err => {});

    };
}

export default angular.module('toBuyLists', [uiRouter, dataService])

    .component('toBuyLists',{
        template,
        controller: ToBuyListsController,
        controllerAs: 'vm',
        bindings: {},
    })

    .name;





