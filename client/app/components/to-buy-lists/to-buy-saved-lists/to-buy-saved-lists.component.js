import angular from 'angular';
import uiRouter from 'angular-ui-router';
import template from './to-buy-saved-lists.html';


class ToBuySavedListsController {
    /*@ngInject*/
    constructor() {

}
}

export default angular.module('toBuySavedLists', [uiRouter])

    .component('toBuySavedLists',{
        template,
        controller: ToBuySavedListsController,
        controllerAs: 'vm',
        bindings: {}
    })

    .name;





