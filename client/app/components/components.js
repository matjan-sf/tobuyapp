import angular from 'angular';
import toBuyNewList from './to-buy-lists/to-buy-new-list/to-buy-new-list.component';
import toBuySavedLists from './to-buy-lists/to-buy-saved-lists/to-buy-saved-lists.component';
import toBuyLists from './to-buy-lists/to-buy-lists.component.js';


let componentModule = angular.module('app.components', [
    toBuyNewList,
    toBuyLists,
    toBuySavedLists
])

    .name;

export default componentModule;
