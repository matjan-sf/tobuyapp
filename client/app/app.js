import angular from 'angular';
import materials from 'angular-material';
import aria from 'angular-aria';
import messages from 'angular-messages';
import animate from 'angular-animate';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';
import dataService from './service/dataService.service';
import Shared from './shared/shared'

angular.module('app', [
    uiRouter,
    Common,
    Components,
    Shared,
    dataService,
    materials,
    aria,
    animate,
    messages
])

    .config(($stateProvider) => {
        "ngInject";
        $stateProvider
            .state('to-buy-lists', {
                url: '/',
                component: 'to-buy-lists'
            })
            .state('to-buy-new-list', {
                url: '/new',
                component: 'to-buy-new-list'
            })
            .state('to-buy-saved-lists', {
                url: '/saved',
                component: 'to-buy-saved-lists'
            })

    })





    // .config(($locationProvider) => {
    //   "ngInject";
    //   // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    //   // #how-to-configure-your-server-to-work-with-html5mode
    //   $locationProvider.html5Mode(true).hashPrefix('!');
    // })

    .component('app', AppComponent);
